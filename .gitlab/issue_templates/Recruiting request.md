<!--
Title format: "Recruiting for #(issue id of your research project)"
Example: "Recruiting for #123"
-->

<!---To learn about the Research Recruitment process please review the Recruiting handbook page at https://about.gitlab.com/handbook/engineering/ux/ux-research-training/recruiting-participants/>--->

## To be completed by the requester

#### Which research study are you recruiting for?
<!---Add a link to your study's issue or epic--->

#### Who are your target participants?
<!---For example: SREs who use Kubernetes; a mix of GitLab and non-GitLab users. Are there underrepresented groups that would like to ensure you speak with, whom and how many?--->

#### What is your target sample size?
<!---For example: If you are conducting user interviews, how many people would you like to speak with?--->

#### What is the link to your screener or survey?
<!---Please note your screener or survey must be entered into Qualtrics before it can be distributed to users.

Need help getting started with your screener? Here's an example of a completed screener - https://gitlab.eu.qualtrics.com/jfe/preview/SV_cT80heuzGIlAdZH?Q_SurveyVersionID=current&Q_CHL=preview--->

***The remaining questions relate to user interviews and usability testing only:***

#### When are you hoping to speak with users?
<!---For example: Over the next 2 weeks, within the next month, etc.--->

#### Do you have any upcoming vacation time?

#### What is the link to your Calendly?
<!---If participants are not being sourced via Respondent, please ensure that you have done the following:
1. Limited your bookable hours: Calendly -> Event types -> 30 min meeting -> When can people book this event? -> Add your available times
2. Specified several different time periods that you'll be able to speak with users.

Please note that if you are booking participants through Respondent, you will be asked to re-enter your availability there, because we are required to book through the platform.
--->

#### What is the incentive amount?
<!--- See the guidance on what to pay participants here: https://about.gitlab.com/handbook/engineering/ux/ux-research-coordination/#thank-you-gifts
--->

## To be completed by the UX Research Coordinator
* [ ] Sense check the screener or survey.
* [ ] Check that the requestor's Calendly link is set-up correctly.
* [ ] Distribute the screener or survey. Remember: With surveys, send to a sample of participants first.
* [ ] Schedule users [if required].

## Invited participants
| Participant | Scheduled | Interviewed | Thank you gift sent? |
| ------ | ------ | ------ | ------ | 
|  | |   | |   |
|  | |   | |   |
|  | |   | |   |

## To be completed by the requester
* [ ] Make a copy of [this Google doc](https://docs.google.com/spreadsheets/d/1NQCefWQ-I4qO0Z2cIVKbNe5oyd1lDghqFdMYQe1pu0k/edit?usp=sharing)
* [ ] Fill out the copied document where relevant.
* [ ] Ensure the relevant UX Research Coordinator can edit the file.
* [ ] Paste the link to the completed, copied document here: 

## To be completed by the UX Research Coordinator
* [ ] Pay users
* [ ] Delete the copied document from Google Drive.

/label ~"UXR recruitment" ~"Research coordination" 
