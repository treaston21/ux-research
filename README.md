## About
You can use this project to propose research and track research in progress. To view completed research, please visit [UXR_Insights](https://gitlab.com/gitlab-org/uxr_insights/) repository.

## How to propose research

If you are a Product Manager, Product Designer or UX Researcher - [Please follow these instructions](https://about.gitlab.com/handbook/engineering/ux/ux-research/#how-ux-research-product-management-and-product-design-work-together-on-research-single-stage-group-initiatives)

If you have a different role at GitLab - [Please follow these instructions](https://about.gitlab.com/handbook/engineering/ux/ux-research/#how-to-request-research)

## GitLab First Look
We want your feedback on how we can continue to improve GitLab. By joining GitLab First Look, you’ll be the first to see new features and your thoughts will drive product improvements for a better user experience.

[Find out more about GitLab First Look](https://about.gitlab.com/community/gitlab-first-look/index.html)

## Contributing

GitLab is an open source project and we are very happy to accept community contributions. Please refer to [CONTRIBUTING.md](/CONTRIBUTING.md) for details.

Please note this project is used for issue tracking only.

## Project Members

Owners: @asmolinski2 and @sarahj

UX Researchers: @katokpara, @tlavi, @loriewhitaker, @jeffcrow, @akiripolsky and @awieckowski

UX Research Coordinators: @evhoffmann

## Links

[UX Research Handbook](https://about.gitlab.com/handbook/engineering/ux/ux-research/)

[UX Department Handbook](https://about.gitlab.com/handbook/engineering/ux/)